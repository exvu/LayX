import App from "./core/App";
import Layx from "./basic/interfaces/Layx";
import * as Types from "../types";

import "./assets/styles";
import "./assets/icons";
function merge(target: any, ...rest: any[]): any {

    for (let source of rest) {
        for (let prop in source) {
            if (source.hasOwnProperty(prop)) {
                let value = source[prop];
                try {
                    if (!(prop in target)) {
                        target[prop] = value;
                    } else {
                        //前后类型是否一致
                        if (typeof target[prop] == "object" && typeof source[prop] == "object") {

                            if (target[prop].constructor == source[prop].constructor) {
                                value = merge({}, value, target[prop]);
                            } else {
                                target[prop] = value;
                            }
                        } else {
                            target[prop] = value;
                        }
                    }


                } catch (e) {
                    target[prop] = value;
                }

            }
        }
    }
    return target;
};
export default (function layx(): Layx {
    const layx = <Layx>function (options: any): void { };

    let globalConfig: Types.WindowOption | null;
    const app = new App(layx);
    layx.v = app.version;
    layx.open = function (options: Types.WindowOption) {
        app.open(merge({}, globalConfig || {}, options));
    }
    layx.config = function (options: Types.WindowOption) {
        globalConfig = options;
    }
    layx.windows = app.windows;
    layx.window = app.window;
    layx.lastWindow = app.lastWindow;
    layx.getWindow = function (id: string) {
        return app.getWindow(id);
    }
    layx.destroy = function (id: string) {
        app.destroy(id);
    }

    layx.notice = function (options: Types.NoticeOption) {
        app.notice(options);
    }
    layx.notices = app.notices;

    return layx;
})();